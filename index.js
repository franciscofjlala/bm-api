/**
 * Express 
 * App Initialization
 */
const express = require('express');
const app = express();

/**
 * Dependencies
 */
const logger = require('./helpers/logger');
const mongo = require('./helpers/mongoose');
const config = require('./config');

const bodyParser = require('body-parser');

/**
 * Routes
 */

const BillRouter = require('./api/routes/BillRouter');
const StoreRouter = require('./api/routes/StoreRouter');
const CategoryRouter = require('./api/routes/CategoryRouter');
const TagsRouter = require('./api/routes/TagsRouter');
const ProviderRouter = require('./api/routes/ProviderRouter');
const ProductRouter = require('./api/routes/ProductRouter');
const StockRouter = require('./api/routes/StockRouter');

/**
 * Vars
 */
const port = process.env.PORT || config.PORT;


// parse application/json
app.use(bodyParser.json())


/**
 * Midllewares
 */

app.use('/bill', BillRouter);
app.use('/store', StoreRouter);
app.use('/category', CategoryRouter);
app.use('/tags', TagsRouter);
app.use('/provider', ProviderRouter);
app.use('/product', ProductRouter);
app.use('/stock', StockRouter);

/**
 * Start App
 */
mongo();
app.listen(port);

logger.info(`API Started on : ${port}`);
