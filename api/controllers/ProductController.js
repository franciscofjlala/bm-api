/**
 * Module Dependencies
 */
const ProductService = require('../services/ProductService');
const logger = require('../../helpers/logger');

/**
 * Controllers
 */

exports.get = function get(req, res, next) {
    ProductService.list()
    .then((product) => {
      logger.success('Product listed successfully');
      res.json(product);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.post = function post(req, res, next) {
  ProductService.create(req.body)
    .then((product) => {
      logger.success('Product created successfully');
      res.json(product);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.getBy = function getBy(req, res, next) {
  ProductService.byId(req.params.id)
    .then((product) => {
      logger.success('Product finded Id successfully');
      res.json(product);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.put = function put(req, res, next) {
  ProductService.update(req.params.id, req.body)
    .then((product) => {
      logger.success('Product updated successfully');
      res.json(product);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.remove = function remove(req, res, next) {
  ProductService.remove(req.params.id)
    .then((product) => {
      logger.success('Product removed successfully');
      res.json(product);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};