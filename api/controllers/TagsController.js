/**
 * Module Dependencies
 */
const TagsService = require('../services/TagsService');
const logger = require('../../helpers/logger');

/**
 * Controllers
 */

exports.get = function get(req, res, next) {
    TagsService.list()
    .then((tags) => {
      logger.success('Tags listed successfully');
      res.json(tags);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.post = function post(req, res, next) {
  TagsService.create(req.body)
    .then((tags) => {
      logger.success('Tags created successfully');
      res.json(tags);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.getBy = function getBy(req, res, next) {
  TagsService.byId(req.params.id)
    .then((tags) => {
      logger.success('Tags finded Id successfully');
      res.json(tags);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.put = function put(req, res, next) {
  TagsService.update(req.params.id, req.body)
    .then((tags) => {
      logger.success('Tags updated successfully');
      res.json(tags);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.remove = function remove(req, res, next) {
  TagsService.remove(req.params.id)
    .then((tags) => {
      logger.success('Tags removed successfully');
      res.json(tags);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};