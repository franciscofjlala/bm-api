/**
 * Module Dependencies
 */
const StockService = require('../services/StockService');
const logger = require('../../helpers/logger');

/**
 * Controllers
 */

exports.get = function get(req, res, next) {
    StockService.list()
    .then((stock) => {
      logger.success('Stock listed successfully');
      res.json(stock);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.post = function post(req, res, next) {
  StockService.create(req.body)
    .then((stock) => {
      logger.success('Stock created successfully');
      res.json(stock);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.getBy = function getBy(req, res, next) {
  StockService.byId(req.params.id)
    .then((stock) => {
      logger.success('Stock finded Id successfully');
      res.json(stock);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.put = function put(req, res, next) {
  StockService.update(req.params.id, req.body)
    .then((stock) => {
      logger.success('Stock updated successfully');
      res.json(stock);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.remove = function remove(req, res, next) {
  StockService.remove(req.params.id)
    .then((stock) => {
      logger.success('Stock removed successfully');
      res.json(stock);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};