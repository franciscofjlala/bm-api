/**
 * Module Dependencies
 */
const CategoryService = require('../services/CategoryService');
const logger = require('../../helpers/logger');

/**
 * Controllers
 */

exports.get = function get(req, res, next) {
    CategoryService.list()
    .then((category) => {
      logger.success('Category listed successfully');
      res.json(category);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.post = function post(req, res, next) {
  CategoryService.create(req.body)
    .then((category) => {
      logger.success('Category created successfully');
      res.json(category);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.getBy = function getBy(req, res, next) {
  CategoryService.byId(req.params.id)
    .then((category) => {
      logger.success('Category finded Id successfully');
      res.json(category);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.put = function put(req, res, next) {
  CategoryService.update(req.params.id, req.body)
    .then((category) => {
      logger.success('Category updated successfully');
      res.json(category);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.remove = function remove(req, res, next) {
  CategoryService.remove(req.params.id)
    .then((category) => {
      logger.success('Category removed successfully');
      res.json(category);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};