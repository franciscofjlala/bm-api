/**
 * Module Dependencies
 */
const StoreService = require('../services/StoreService');
const logger = require('../../helpers/logger');

/**
 * Controllers
 */

exports.get = function get(req, res, next) {
    StoreService.list()
    .then((store) => {
      logger.success('Store listed successfully');
      res.json(store);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.post = function post(req, res, next) {
  StoreService.create(req.body)
    .then((store) => {
      logger.success('Store created successfully');
      res.json(store);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.getBy = function getBy(req, res, next) {
  StoreService.byId(req.params.id)
    .then((store) => {
      logger.success('Store finded Id successfully');
      res.json(store);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.put = function put(req, res, next) {
  StoreService.update(req.params.id, req.body)
    .then((store) => {
      logger.success('Store updated successfully');
      res.json(store);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.remove = function remove(req, res, next) {
  StoreService.remove(req.params.id)
    .then((store) => {
      logger.success('Store removed successfully');
      res.json(store);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};