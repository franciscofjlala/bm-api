/**
 * Module Dependencies
 */
const BillService = require('../services/BillService');
const logger = require('../../helpers/logger');

/**
 * Controllers
 */

exports.get = function get(req, res, next) {
  BillService.list()
    .then((bill) => {
      logger.success('Bill listed successfully');
      res.json(bill);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.post = function post(req, res, next) {
  BillService.create(req.body)
    .then((bill) => {
      logger.success('Bill created successfully');
      res.json(bill);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.getBy = function getBy(req, res, next) {
  BillService.byId(req.params.id)
    .then((bill) => {
      logger.success('Bill finded Id successfully');
      res.json(bill);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.put = function put(req, res, next) {
  BillService.update(req.params.id, req.body)
    .then((bill) => {
      logger.success('Bill updated successfully');
      res.json(bill);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.remove = function remove(req, res, next) {
  BillService.remove(req.params.id)
    .then((bill) => {
      logger.success('Bill removed successfully');
      res.json(bill);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};