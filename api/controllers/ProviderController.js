/**
 * Module Dependencies
 */
const ProviderService = require('../services/ProviderService');
const logger = require('../../helpers/logger');

/**
 * Controllers
 */

exports.get = function get(req, res, next) {
    ProviderService.list()
    .then((provider) => {
      logger.success('Provider listed successfully');
      res.json(provider);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.post = function post(req, res, next) {
  ProviderService.create(req.body)
    .then((provider) => {
      logger.success('Provider created successfully');
      res.json(provider);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.getBy = function getBy(req, res, next) {
  ProviderService.byId(req.params.id)
    .then((provider) => {
      logger.success('Provider finded Id successfully');
      res.json(provider);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.put = function put(req, res, next) {
  ProviderService.update(req.params.id, req.body)
    .then((provider) => {
      logger.success('Provider updated successfully');
      res.json(provider);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};

exports.remove = function remove(req, res, next) {
  ProviderService.remove(req.params.id)
    .then((provider) => {
      logger.success('Provider removed successfully');
      res.json(provider);
    })
    .catch(err => {
      logger.error(err);
      res.send(err);
    });
};