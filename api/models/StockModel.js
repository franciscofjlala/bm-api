const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StockModel = new Schema({
  product: {
    type: Schema.ObjectId,
    ref: 'Product',
  },
  movement: {
    type: Number,
  },
  movement_type: {
    type: String,
    enum: [
      'in',
      'out',
    ],
    default: ['in'],
  },
  price: {
    type: Number,
  },
});

module.exports = mongoose.model('Stock', StockModel);