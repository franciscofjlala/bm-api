const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProviderModel = new Schema({
  name: {
      type: String,
  },
  phone: {
      type: Number,
  },
  mail: {
      type: String,
  },
  info: {
      type: Object,
  },
});

module.exports = mongoose.model('Provider', ProviderModel);