const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BillModel = new Schema({
  number: {
    type: Number,
    Required: 'Por favor ingrese el número de boleta',
  },
  amount: {
    type: Number,
    Required: 'Por favor ingrese el monto a abonar',
  },
  date_created: {
    type: Date,
    default: Date.now,
  },
  due_date: {
    type: Date,
  },
  status: {
    type: [{
      type: String,
      enum: ['unpaid', 'paid'],
    }],
    default: ['unpaid']
  },
  items: [{
    type: Schema.ObjectId,
    ref: 'Stock'
  }],
  provider: {
    type: Schema.ObjectId,
    ref: 'Provider'
  },
  store: {
    type: Schema.ObjectId,
    ref: 'Store'
  },
  tags: [{
    type: Schema.ObjectId,
    ref: 'Tags'
  }],
});

module.exports = mongoose.model('Bill', BillModel);