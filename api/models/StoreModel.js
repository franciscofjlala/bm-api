const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StoreModel = new Schema({
  name: {
      type: String,
  },
  phone: {
      type: Number,
  },
  info: {
      type: Object,
  },
});

module.exports = mongoose.model('Store', StoreModel);