const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductModel = new Schema({
  name: {
    type: String,
  },
  price: {
    type: Number,
  },
  measuring_unit: {
    type: String,
  },
  in_stock: {
    type: Number,
  },
  sku: {
    type: Number,
  },
  category: {
    type: Schema.ObjectId,
    ref: 'Category',
  },
});

module.exports = mongoose.model('Product', ProductModel);