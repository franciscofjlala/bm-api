/**
 * Model
 */
const BillModel = require('../models/BillModel');
const StockService = require('../services/StockService');
const ProductService = require('../services/ProductService');

/**
 * Service Definition
 */

class BillService {
  static list() {
    const Bill = BillModel;
    return Bill.find({})
      .populate('store items tags provider')
      .populate({
        path: 'items',
        populate: {
          path: 'product',
          model: 'Product',
        },
      });
  }

  static create(body) {
    return StockService.createMultiple(body.items)
      .then(result => {
        let items = result.map(item => {
          return item._id;
        });

        body.items = items;

        return ProductService.updateStock(result)
          .then(res => {
            const Bill = new BillModel(body);
            return Bill.save();
          });
      });
  }

  static byId(id) {
    const Bill = BillModel;
    return Bill.findById({
        _id: id,
    });
  }

  static update(id, body) {
    const Bill = BillModel;
    return Bill.findOneAndUpdate(
      { _id: id },
      body,
      { upsert: true, new: true }
    );
  }

  static remove(id) {
    const Bill = BillModel;
    return Bill.remove({
        _id: id,
    });
  }
}

/**
 * Expose Service
 */

module.exports = BillService;
