/**
 * Model
 */
const CategoryModel = require('../models/CategoryModel');

/**
 * Service Definition
 */

class CategoryService {
  static list() {
    const Category = CategoryModel;
    return Category.find({});
  }

  static create(body) {
    const Category = new CategoryModel(body);
    return Category.save();
  }

  static byId(id) {
    const Category = CategoryModel;
    return Category.findById({
        _id: id,
    });
  }

  static update(id, body) {
    const Category = CategoryModel;
    return Category.findOneAndUpdate(
      { _id: id },
      body,
      { upsert: true, new: true }
    );
  }

  static remove(id) {
    const Category = CategoryModel;
    return Category.remove({
        _id: id,
    });
  }
}

/**
 * Expose Service
 */

module.exports = CategoryService;
