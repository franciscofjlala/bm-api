/**
 * Model
 */
const ProductModel = require('../models/ProductModel');
const logger = require('../../helpers/logger');

/**
 * Service Definition
 */

class ProductService {
  static list() {
    const Product = ProductModel;
    return Product.find({})
      .populate('category');
  }

  static create(body) {
    const Product = new ProductModel(body);
    return Product.save();
  }

  static byId(id) {
    const Product = ProductModel;
    return Product.findById({
        _id: id,
    });
  }

  static update(id, body) {
    const Product = ProductModel;
    return Product.findOneAndUpdate(
      { _id: id },
      body,
      { upsert: true, new: true }
    );
  }

  static remove(id) {
    const Product = ProductModel;
    return Product.remove({
        _id: id,
    });
  }

  static updateStock(products) {
    const Product = ProductModel;
    const resolvedProducts = [];
    return new Promise((resolve, reject) => {
      products.map(product => {
        return Product.findOneAndUpdate(
          { _id: product.product },
          { $inc: { in_stock: product.movement }},
          { upsert: true }
        ).then(res => {
          resolvedProducts.push(product);
          resolve(resolvedProducts);
          logger.success('Updated Product stock');
        });
      });
    });
  }
}

/**
 * Expose Service
 */

module.exports = ProductService;
