/**
 * Model
 */
const ProviderModel = require('../models/ProviderModel');

/**
 * Service Definition
 */

class ProviderService {
  static list() {
    const Provider = ProviderModel;
    return Provider.find({});
  }

  static create(body) {
    const Provider = new ProviderModel(body);
    return Provider.save();
  }

  static byId(id) {
    const Provider = ProviderModel;
    return Provider.findById({
        _id: id,
    });
  }

  static update(id, body) {
    const Provider = ProviderModel;
    return Provider.findOneAndUpdate(
      { _id: id },
      body,
      { upsert: true, new: true }
    );
  }

  static remove(id) {
    const Provider = ProviderModel;
    return Provider.remove({
        _id: id,
    });
  }
}

/**
 * Expose Service
 */

module.exports = ProviderService;
