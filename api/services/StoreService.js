/**
 * Model
 */
const StoreModel = require('../models/StoreModel');

/**
 * Service Definition
 */

class StoreService {
  static list() {
    const Store = StoreModel;
    return Store.find({});
  }

  static create(body) {
    const Store = new StoreModel(body);
    return Store.save();
  }

  static byId(id) {
    const Store = StoreModel;
    return Store.findById({
        _id: id,
    });
  }

  static update(id, body) {
    const Store = StoreModel;
    return Store.findOneAndUpdate(
      { _id: id },
      body,
      { upsert: true, new: true }
    );
  }

  static remove(id) {
    const Store = StoreModel;
    return Store.remove({
        _id: id,
    });
  }
}

/**
 * Expose Service
 */

module.exports = StoreService;
