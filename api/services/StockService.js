/**
 * Model
 */
const StockModel = require('../models/StockModel');
const ProductService = require('../services/ProductService');

/**
 * Service Definition
 */

class StockService {
  static list() {
    const Stock = StockModel;
    return Stock.find({})
      .populate('product');
  }

  static create(body) {
    const Stock = new StockModel(body);
    const Product = ProductService;

    return Stock.save()
      .then(result => {
        return Product.updateStock([result]);
      });
  }

  static byId(id) {
    const Stock = StockModel;
    return Stock.findById({
        _id: id,
    });
  }

  static update(id, body) {
    const Stock = StockModel;
    return Stock.findOneAndUpdate(
      { _id: id },
      body,
      { upsert: true, new: true }
    );
  }

  static remove(id) {
    const Stock = StockModel;
    return Stock.remove({
        _id: id,
    });
  }

  static createMultiple(body) {
    const Stock = StockModel;
    return Stock.insertMany(body);
  }
}

/**
 * Expose Service
 */

module.exports = StockService;
