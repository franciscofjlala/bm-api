/**
 * Model
 */
const TagsModel = require('../models/TagsModel');

/**
 * Service Definition
 */

class TagsService {
  static list() {
    const Tags = TagsModel;
    return Tags.find({});
  }

  static create(body) {
    const Tags = new TagsModel(body);
    return Tags.save();
  }

  static byId(id) {
    const Tags = TagsModel;
    return Tags.findById({
        _id: id,
    });
  }

  static update(id, body) {
    const Tags = TagsModel;
    return Tags.findOneAndUpdate(
      { _id: id },
      body,
      { upsert: true, new: true }
    );
  }

  static remove(id) {
    const Tags = TagsModel;
    return Tags.remove({
        _id: id,
    });
  }
}

/**
 * Expose Service
 */

module.exports = TagsService;
