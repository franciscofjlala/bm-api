const app = require('express')();
const { 
  get,
  post,
  put,
  remove,
  getBy,
} = require('../controllers/TagsController');

app.get('/', get);
app.post('/', post);
app.put('/:id', put);
app.delete('/:id', remove);
app.get('/:id', getBy);

module.exports = app;
