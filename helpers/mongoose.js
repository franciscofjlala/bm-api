const mongoose = require('mongoose');

const config = require('../config');
const logger = require('../helpers/logger');

module.exports = () => {
  mongoose.Promise = global.Promise;
  mongoose.createConnection();
  const mongoDB = mongoose.connection;

  mongoDB.openUri(config.MONGO.URL);
  console.log(mongoDB.readyState)

  mongoDB.on('error', () => { logger.error('Cant connect to MongoDB') } );

  return mongoDB;
};
