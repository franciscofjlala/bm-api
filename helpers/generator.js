const fs = require('fs');
const handlebars = require('handlebars');


const name = process.argv[2];
const data = {
  name: name,
  nameLower: name.toLowerCase(),
};

const generateController = () => {
  fs.readFile('./helpers/template/Controller.hbs', 'utf-8', (err, source) => {
    const template = handlebars.compile(source);
    const file = template(data);
    
    fs.writeFile(`./api/controllers/${name}Controller.js`, file, 'utf-8');
  });
};

const generateModel = () => {
  fs.readFile('./helpers/template/Model.hbs', 'utf-8', (err, source) => {
    const template = handlebars.compile(source);
    const file = template(data);
    
    fs.writeFile(`./api/models/${name}Model.js`, file, 'utf-8');
  });
};

const generateRouter = () => {
  fs.readFile('./helpers/template/Router.hbs', 'utf-8', (err, source) => {
    const template = handlebars.compile(source);
    const file = template(data);
    
    fs.writeFile(`./api/routes/${name}Router.js`, file, 'utf-8');
  });
};

const generateService = () => {
  fs.readFile('./helpers/template/Service.hbs', 'utf-8', (err, source) => {
    const template = handlebars.compile(source);
    const file = template(data);
    
    fs.writeFile(`./api/services/${name}Service.js`, file, 'utf-8');
  });
};

generateController();
generateModel();
generateRouter();
generateService();
