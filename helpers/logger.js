const colors = require('colors');

class Logger {
  static info (message) {
    console.info(`[Info] [${new Date()}] ${message}`.blue);
  }

  static warning (message) {
    console.warning(`[Warning] [${new Date()}] ${message}`.yellow);
  }

  static error (message) {
    console.error(`[Error] [${new Date()}] ${message}`.red);
  }

  static success (message) {
    console.info(`[Success] [${new Date()}] ${message}`.green);
  }

  static log (message) {
    console.log(`[Log] [${new Date()}] ${message}`.white);
  }
}

module.exports = Logger;
