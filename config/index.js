const CONFIG = {
    PORT: 3000,
    MONGO: {
        URL: 'mongodb://localhost/bm',
    },
};

module.exports = CONFIG;
